<?php

namespace Drupal\views_style_foundation_grid\Plugin\views\style;

use Drupal\Component\Utility\Html;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\style\StylePluginBase;

/**
 * Foundation Grid style plugin.
 *
 * @ViewsStyle(
 *   id = "views_style_foundation_grid",
 *   title = @Translation("Foundation Grid"),
 *   help = @Translation("Provides a ZURB Foundation Grid views style."),
 *   theme = "views_view_foundation_grid",
 *   display_types = {"normal"}
 * )
 */
class FoundationGrid extends StylePluginBase {

  /**
   * Whether or not this style uses a row plugin.
   *
   * @var bool
   */
  protected $usesRowPlugin = TRUE;

  /**
   * Does the style plugin support custom css class for the rows.
   *
   * @var bool
   */
  protected $usesRowClass = FALSE;

  /**
   * {@inheritdoc}
   */
  protected function defineOptions(): array {
    $options = parent::defineOptions();

    // Add our settings:
    $options['grouping_title_type'] = ['default' => 'h3'];
    $options['grouping_title_class_custom'] = ['default' => ''];
    $options['orientation'] = ['default' => 'horizontal'];
    $options['grid_gutter_vertical'] = ['default' => TRUE];
    $options['grid_gutter_horizontal'] = ['default' => TRUE];
    $options['grid_gutter_type'] = ['default' => 'margin'];
    $options['grid_class_custom'] = ['default' => ''];
    $options['cells_small'] = ['default' => '12'];
    $options['cells_medium'] = ['default' => '6'];
    $options['cells_large'] = ['default' => '4'];
    $options['cell_class_custom'] = ['default' => ''];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(mixed &$form, FormStateInterface $form_state): void {
    parent::buildOptionsForm($form, $form_state);

    $form['grouping_title_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Grouping title type'),
      '#options' => [
        'h1' => 'h1',
        'h2' => 'h2',
        'h3' => 'h3',
        'h4' => 'h4',
        'h5' => 'h5',
        'span' => 'span',
        'div' => 'div',
        'strong' => 'strong',
        'em' => 'em',
      ],
      '#default_value' => $this->options['grouping_title_type'],
      '#states' => [
        'invisible' => [
          ':input[name="style_options[grouping][0][field]"]' => ['value' => ''],
        ],
      ],
    ];

    $form['grouping_title_class_custom'] = [
      '#title' => $this->t('Grouping title custom classes'),
      '#description' => $this->t('Allows to set additional title classes.'),
      '#type' => 'textfield',
      '#size' => '30',
      '#default_value' => $this->options['grouping_title_class_custom'],
      '#states' => [
        'invisible' => [
          ':input[name="style_options[grouping][0][field]"]' => ['value' => ''],
        ],
      ],
    ];

    $form['orientation'] = [
      '#type' => 'radios',
      '#title' => $this->t('Orientation'),
      '#options' => [
        'horizontal' => $this->t('Horizontal'),
        'vertical' => $this->t('Vertical'),
      ],
      '#default_value' => $this->options['orientation'],
      '#description' => $this->t('Horizontal orientation will place items starting in the upper left and moving right. Vertical orientation will place items starting in the upper left and moving down.'),
    ];

    $form['grid_gutter_horizontal'] = [
      '#title' => $this->t('Grid Gutter Horizontal'),
      '#description' => $this->t('Enable horizontal grid gutter.'),
      '#type' => 'checkbox',
      '#default_value' => $this->options['grid_gutter_horizontal'],
    ];

    $form['grid_gutter_vertical'] = [
      '#title' => $this->t('Grid Gutter Vertical'),
      '#description' => $this->t('Enable vertical grid gutter.'),
      '#type' => 'checkbox',
      '#default_value' => $this->options['grid_gutter_vertical'],
    ];

    $form['grid_gutter_type'] = [
      '#type' => 'radios',
      '#title' => $this->t('Grid Gutter Type'),
      '#options' => [
        'margin' => $this->t('Margin'),
        'padding' => $this->t('Padding'),
      ],
      '#default_value' => $this->options['grid_gutter_type'],
      '#description' => $this->t('Choose to use margin or padding for gutters.'),
    ];

    $form['grid_class_custom'] = [
      '#title' => $this->t('Custom grid classes'),
      '#description' => $this->t('Allows to set additional custom grid classes around the cells, like grid-frame.'),
      '#type' => 'textfield',
      '#size' => '30',
      '#default_value' => $this->options['grid_class_custom'],
    ];

    $cells_options = [
      '' => 'None',
      'auto' => 'Auto',
      '1' => '1',
      '2' => '2',
      '3' => '3',
      '4' => '4',
      '5' => '5',
      '6' => '6',
      '7' => '7',
      '8' => '8',
      '9' => '9',
      '10' => '10',
      '11' => '11',
      '12' => '12',
    ];
    $cells_description = $this->t('Grid columns consumed per cell (x/12). "Auto" takes up the remaining space. "None" sets no explicit size, to use inheritance from smaller sizes.');
    $form['cells_small'] = [
      '#type' => 'select',
      '#title' => $this->t('Cells Grid Columns on small devices.'),
      '#options' => $cells_options,
      '#default_value' => $this->options['cells_small'],
      '#description' => $cells_description,
    ];

    $form['cells_medium'] = [
      '#type' => 'select',
      '#title' => $this->t('Cells Grid Columns on medium devices.'),
      '#options' => $cells_options,
      '#default_value' => $this->options['cells_medium'],
      '#description' => $cells_description,
    ];

    $form['cells_large'] = [
      '#type' => 'select',
      '#title' => $this->t('Cells Grid Columns on large devices.'),
      '#options' => $cells_options,
      '#default_value' => $this->options['cells_large'],
      '#description' => $cells_description,
    ];

    $form['cell_class_custom'] = [
      '#title' => $this->t('Custom Cell Classes'),
      '#description' => $this->t('Allows to set additional custom classes on cells.'),
      '#type' => 'textfield',
      '#size' => '30',
      '#default_value' => $this->options['cell_class_custom'],
    ];
    if ($this->usesFields()) {
      $form['cell_class_custom']['#description'] .= ' ' . $this->t('You may use field tokens from as per the "Replacement patterns" used in "Rewrite the output of this field" for all fields.');
    }
  }

  /**
   * Return the token-replaced cell classes for the specified result.
   *
   * @param int $result_index
   *   The delta of the result item to get custom classes for.
   *
   * @return string
   *   A space-delimited string of classes.
   */
  public function getCustomCellClass($result_index) {
    $class = $this->options['cell_class_custom'];
    if ($this->usesFields() && $this->view->field) {
      $class = strip_tags($this->tokenizeValue($class, $result_index));
    }

    $classes = explode(' ', $class);
    foreach ($classes as &$class) {
      $class = Html::cleanCssIdentifier($class);
    }
    return implode(' ', $classes);
  }

}
